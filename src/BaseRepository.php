<?php

namespace Belo;

abstract class BaseRepository
{
    /**
     * The model instance of the repository
     * @var [type]
     */
    protected $model;

    /**
     * Create a new instance of BaseRepository
     */
    public function __construct()
    {
        $this->makeModel();
    }

    /**
     * Returns the instance of the model
     *
     * @return [type] [description]
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Define the relation of this repository to the model
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    abstract function model();

    /**
     * Create a new instance of model
     */
    public function makeModel()
    {
        $model = $this->model();

        return $this->model = new $model;
    }

    /**
     * Update the model
     *
     * @param $data
     * @param $id
     * @return mixed
     */
    public function update($data, $id)
    {
        $model = $this
            ->model
            ->find($id);

        $model
            ->update($data);

        return $model;
    }

    /**
     * Save Model
     * @param $data
     * @return mixed
     */
    public function save($data)
    {
        return $this
            ->model
            ->create($data);
    }
}