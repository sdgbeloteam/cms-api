<?php

namespace Belo\PrivacyPolicy;

use Illuminate\Database\Eloquent\Model;

class PrivacyPolicy extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'app_privacy_policy';

    /**
     * The primary key for the model.
     * @var string
     */
    protected $primaryKey = 'created_at';
    
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'content',
        'updated_at',
    ];

    
    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;
}