<?php

$app->add(new \Slim\Middleware\JwtAuthentication([
    'attribute' => 'jwt',
    'secure' => false,
    'secret' => env('JWT_SECRET', 'Q0Bn5s1lT6kd7U1QlSEh1XJEwOq0P7fS'),
    'path' => ['/cms-api'],
    'passthrough' => ['/cms-api/auth/login'],
    "callback" => function ($request, $response, $arguments) use ($container) {
        $container['jwt.decoded'] = $arguments['decoded'];
    },
    'error' => function ($request, $response, $arguments) {
        $data["status"] = "error";
        $data["message"] = $arguments["message"];

        return $response
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data));
    }
]));

$app->add(new \Tuupola\Middleware\Cors([
    'origin' => ['*'],
    'methods' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
    'headers.allow' => ['Content-Type',' Accept', 'Authorization', 'X-Requested-With', 'Origin'],
    'headers.expose' => [],
    'credentials' => false,
    'cache' => 0,
]));

