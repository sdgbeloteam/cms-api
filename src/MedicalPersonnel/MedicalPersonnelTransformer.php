<?php

namespace Belo\MedicalPersonnel;

use League\Fractal\TransformerAbstract;

class MedicalPersonnelTransformer extends TransformerAbstract
{
    /**
     * Transform the response from the database to a standard API request
     * @param  medicalPersonnel [description]
     * @return [type]           [description]
     */
    public function transform(MedicalPersonnel $personnel)
    {   
        return [
            'code' => $personnel->id,
            'name' => $personnel->name,
            'education' => $personnel->education,
            'specialization' => $personnel->specialization,
            'branch_code' => ($personnel->branch_code ? $personnel->branch_code : null),
            'avatar' => ($personnel->avatar) ? $personnel->avatar: null,
            'branch_name' => ($personnel->branchAssigned ? $personnel->branchAssigned->name : null)
        ];
    }
}