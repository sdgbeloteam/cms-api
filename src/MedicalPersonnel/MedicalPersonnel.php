<?php

namespace Belo\MedicalPersonnel;

use Illuminate\Database\Eloquent\Model;

class MedicalPersonnel extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'medical_personnel';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'avatar',
        'education',
        'specialization',
    ];

	/**
     * [branch description]
     * @return [type] [description]
     */
    public function branchAssigned()
    {
        return $this->belongsTo(\Belo\Branch\Branch::class, 'branch_code', 'id');
    }
}