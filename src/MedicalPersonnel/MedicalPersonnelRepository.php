<?php

namespace Belo\MedicalPersonnel;

use Belo\BaseRepository;
use Belo\MedicalPersonnel\MedicalPersonnel;
use Illuminate\Database\EloquentModel;

class MedicalPersonnelRepository extends BaseRepository
{
    /**
     * Define the relation of this repository to the model
     * @return Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return MedicalPersonnel::class;
    }

    /**
     * [paginate description]
     * @param  integer $limit [description]
     * @return [type]         [description]
     */
    public function paginate($limit = 20, $page = 1)
    {
        return $this->model
            ->with('branchAssigned')
            ->select([
                'id',
                'branch_code',
                'name',
                'gender',
                'type',
                'email_address',
                'avatar'
            ])
            ->paginate($limit, null, 'page', $page);
    }

    public function getAllPersonnel()
    {
        return $this->model
            ->with('branchAssigned')
            ->select([
                'id',
                'branch_code',
                'name',
                'gender',
                'type',
                'email_address',
                'avatar'
            ])
            ->get();
    }

    public function findById($code)
    {
        return $this
            ->model
			->with('branchAssigned')
			->where('id', $code)
            ->first();
    }

    public function findByBranch($branch)
    {
        return $this->model
            ->with('branchAssigned')
            ->select([
                'id',
                'branch_code',
                'name',
                'education',
                'type',
                'specialization',
                'avatar'
            ])
			->where('branch_code', $branch)
            ->get();
    }

    public function update($data, $id)
    {
        $model = $this
            ->model
            ->where('id', $id)
            ->first();
        // dd($model);
        $model->update([
            'avatar' => isset($data['avatar']) ? $data['avatar'] : $model['avatar'],
            'specialization' => isset($data['specialization']) ? $data['specialization'] : $model['specialization'],
            'education' => isset($data['education']) ? $data['education'] : $model['education'],
        ]);
        
        return $model;
    }

    public function searchPersonnel($query)
    {
        $queryModel = $this
            ->model
            ->with('branchAssigned');

        if ($query) {
            $queryModel->where('name', 'like', "%$query%");
        }

        return $queryModel->get();
    }
}
