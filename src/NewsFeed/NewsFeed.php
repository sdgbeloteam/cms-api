<?php

namespace Belo\NewsFeed;

use Illuminate\Database\Eloquent\Model;

class NewsFeed extends Model
{
    /**
     * The table associated with the model.
     * @var int
     */
    protected $table = 'app_news_feeds';
    
    /**
     * The primary key for the model.
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'image_link',
        'news_status',
    ];

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;
}
