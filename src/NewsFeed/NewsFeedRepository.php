<?php

namespace Belo\NewsFeed;

use Belo\BaseRepository;
use Belo\NewsFeed\NewsFeed;
use Illuminate\Database\EloquentModel;

class NewsFeedRepository extends BaseRepository
{
    /**
     * Define the relation of this repository to the model
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return NewsFeed::class;
    }

    /**
     * Find medical personnel by id
     * @param  char $id
     * @return Patient
     */
    public function findByNewsFeedId($id)
    {
        return $this
            ->model
            ->where('id', $id)
            ->first();
    }

    public function update($data, $id)
    {
        $model = $this
            ->model
            ->where('id', $id)
            ->first();

        $model->update([
            'image_link' => isset($data['image_link']) ? $data['image_link'] : $model['image_link'],
            'news_status' => isset($data['news_status']) ? $data['news_status'] : $model['news_status'],
        ]);

        return $model;
    }

    public function save($data)
    {
        return $this
        ->model
        ->create($data);
    }
}