<?php

namespace Belo\NewsFeed;

use League\Fractal\TransformerAbstract;

class NewsFeedTransformer extends TransformerAbstract
{
    /**
     * Transform the response from the database to a standard API request
     * @param  newsFeed [description]
     * @return [type]           [description]
     */
    public function transform(NewsFeed $newsFeed)
    {

        return [
            'id' => $newsFeed->id,
            'news_title' => $newsFeed->news_title,
            'image_link' => $newsFeed->image_link,
            'date_added' => $newsFeed->date_added,
            'url_link' => $newsFeed->url_link,
            'news_status' => $newsFeed->news_status,
        ];
    }
}