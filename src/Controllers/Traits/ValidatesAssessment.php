<?php

namespace Belo\Controllers\Traits;

use Belo\Note\NoteItemAssessment;
use Psr\Http\Message\ResponseInterface;
use Belo\Requests\NoteItemAssessmentRequest;
use Psr\Http\Message\ServerRequestInterface;

trait ValidatesAssessment
{
    /**
     * [validateAssessment description]
     * @param  ServerRequestInterface $request [description]
     * @return [type]                          [description]
     */
    public function validateAssessment(ServerRequestInterface $request)
    {
        if ($errors = $this->validate($request, new NoteItemAssessmentRequest))
        {
            return $errors;
        }
    }
}