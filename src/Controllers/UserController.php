<?php

namespace Belo\Controllers;

use Belo\User\User;
use Belo\User\UserTransformer;
use Belo\User\UserRepository;
use Belo\Requests\UserRequests;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Belo\Uploader\UploadedFile;

class UserController extends Controller
{
    /**
     * [$userRepository description]
     * @var [type]
     */
    protected $userRepository;
     /**
     * Create an instance of UserController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->userRepository = new UserRepository;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $this->paginator(
            $this->userRepository->paginate(
                20,
                $request->getQueryParam('page'),
                $request->getQueryParam('query')
        ), new UserTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $resource = $this->userRepository->findByUsername($args['id']);

        if ($resource) {
            return $this->item(
                $resource, new UserTransformer
            );
        }

        return $this->errorNotFound();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return \Illuminate\Http\Response
     */
    public function store(ServerRequestInterface $request, ResponseInterface $response)
    {
        if ($errors = $this->validate($request, new UserRequests)) {
            return $errors;
        }

        $data = $request->getParsedBody();
        
        // Only upload a profile picture file when the param is avatar
        if($request->getParam('avatar')) {
            try {
                $file = new UploadedFile(
                    $this->container,
                    $request->getParsedBodyParam('avatar'),
                    storage_path('user-profile-picture')
                );

                $file->save();
                $data['avatar'] = $file->getFileName();
            } catch (NotWritableException $e) {
                return $this->errorInternalServerError($e->getMessage());
            }
        }

        $resource = $this
            ->userRepository
            ->save($data);

        return $this->item($resource, new UserTransformer);
    }

    public function update(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        if(!$args['id']){
            return $this->errorBadRequests();
        }

        $data = $request->getParsedBody();

        if($request->getParam('avatar')) {
            try {
                $file = new UploadedFile(
                    $this->container,
                    $request->getParsedBodyParam('avatar'),
                    storage_path('user-profile-picture')
                );

                $file->save();
                $data['avatar'] = $file->getFileName();
            } catch (NotWritableException $e) {
                return $this->errorInternalServerError($e->getMessage());
            }
        }

        $resource = $this->userRepository->updateUser($data, $args['id']);

        return $this->item($resource, new UserTransformer);
    }
}
