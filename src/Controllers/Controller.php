<?php

namespace Belo\Controllers;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use Belo\Requests\RequestInterface;
use League\Fractal\Resource\Collection;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use League\Fractal\Resource\ResourceInterface;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class Controller
{
    /**
     * [$container description]
     * @var [type]
     */
    protected $container;

    /**
     * [$auth description]
     * @var [type]
     */
    protected $auth;

    /**
     * [__construct description]
     * @param [type] $container [description]
     */
    public function __construct($container)
    {
        $this->container = $container;
        $this->auth = $container->auth;
    }

    /**
     * Validate the request against the rules specified
     *
     * @param  ServerRequestInterface $input
     * @param  array $rules
     *
     * @return json
     */
    public function validate(ServerRequestInterface $request, $rules, $messages = [])
    {
        if ($rules instanceof RequestInterface) {
            $rules = $rules->rules();
        }

        $validator = $this
            ->container
            ->get('validator')
            ->make(
                $request->getParsedBody(),
                $rules,
                $messages
            );

        if ($validator->fails()) {
            return $this->errorUnprocessableEntity($validator);
        }
    }

    /**
     * Respond a single entry response
     *
     * @param  Illuminate\Database\Eloquent\Model $model
     * @param  League\Fractal\TransformerAbstract $transformer
     *
     * @return json
     */
    protected function item($model, $transformer, $meta = [])
    {
        $item = new Item($model, $transformer);

        $item->setMeta($meta);

        return $this->jsonResponse($item);
    }

    /**
     * Respond a single entry response
     *
     * @param  Illuminate\Database\Eloquent\Model $model
     * @param  League\Fractal\TransformerAbstract $transformer
     * @return json
     */
    protected function collection($model, $transformer, $meta = [])
    {
        $collection = new Collection($model, $transformer);

        $collection->setMeta($meta);

        return $this->jsonResponse($collection);
    }

    /**
     * Return a paginator response
     *
     * @param  Illuminate\Database\Eloquent\Model $model
     * @param  League\Fractal\TransformerAbstract $transformer
     * @return json
     */
    protected function paginator($model, $transformer, $meta = [])
    {
        $collection = new Collection($model->getCollection(), $transformer);

        $collection->setMeta($meta);

        $collection->setPaginator(new IlluminatePaginatorAdapter($model));

        return $this->jsonResponse($collection);
    }

    /**
     * [jsonResponse description]
     * @param  [type] $resource [description]
     * @return [type]           [description]
     */
    public function jsonResponse($resource, $statusCode = 200)
    {
        if ($resource instanceof ResourceInterface) {
            $fractal = new Manager;

            $resource = $fractal
                ->createData($resource)
                ->toArray();
        }

        $response = $this->container->get('response');

        return $response->withJson($resource, $statusCode);
    }

    /**
     * Throws an error for user bad input
     * @param  $validator
     * @return json
     */
    protected function errorUnprocessableEntity($validator)
    {
        return $this->jsonResponse([
            'message' => '422 Unprocessable Entity',
            'errors' => $validator->errors()->getMessages()
        ], 422);
    }

    /**
     * Show no content
     */
    public function noContent()
    {
        return $this->jsonResponse(null, 204);
    }

    /**
     * Throws an error not found exception
     *
     * @param  string  $message
     * @param  integer $statusCode
     * @return json
     */
    public function errorNotFound($message = 'Not Found', $statusCode = 404)
    {
        return $this->jsonResponse([
            'message' => $message,
            'status_code' => $statusCode
        ], $statusCode);
    }

    /**
     * Throws a bad request exception
     *
     * @param  string $message
     * @param  integer  $statusCode
     * @return json
     */
    public function errorBadRequest($message = 'Bad Request', $statusCode = 400)
    {
        return $this->jsonResponse([
            'message' => $message,
            'status_code' => $statusCode
        ], $statusCode);
    }

    /**
     * Throws an internal server error
     * @param  string $message
     * @param  integer $statusCode
     * @return json
     */
    public function errorInternalServerError($message = 'Internal Server Error', $statusCode = 500)
    {
        return $this->jsonResponse([
            'message' => $message,
            'status_code' => $statusCode
        ], $statusCode);
    }

    /**
     * Throws an error for authentication of user
     * @param  $validator
     * @return json
     */
    public function errorForbidden($message = 'Invalid username or password', $statusCode = 403)
    {
        return $this->jsonResponse([
            'message' => $message,
            'status_code' => $statusCode
        ], $statusCode);
    }
}
