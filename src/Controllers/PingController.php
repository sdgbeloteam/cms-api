<?php

namespace Belo\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class PingController extends Controller
{
    /**
     *
     * Send a pong response to the user
     * @return [type] [description]
     */
    public function ping()
    {
        $versionFile = __DIR__ . '/../../.version';

        return $this->jsonResponse([
            'version' => trim(file_get_contents($versionFile))
        ]);
    }
}
