<?php

namespace Belo\Controllers;

use Belo\MedicalPersonnel\MedicalPersonnel;
use Belo\MedicalPersonnel\MedicalPersonnelRepository;
use Belo\MedicalPersonnel\MedicalPersonnelTransformer;
use Belo\Requests\MedicalPersonnelRequests;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Belo\Uploader\UploadedFile;

class MedicalPersonnelController extends Controller
{
    /**
     * [$medicalPersonnelRepository description]
     * @var [type]
     */
    public $medicalPersonnelRepository;

    /**
     * Create an instance of BranchControllers
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->medicalPersonnelRepository = new MedicalPersonnelRepository;
    }

    public function index(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $personnel = $this->medicalPersonnelRepository->getAllPersonnel();
        return $this->collection($personnel, new MedicalPersonnelTransformer);
    }

    public function show(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $personnel = $this->medicalPersonnelRepository->findById($args['id']);

        if($personnel){
            return $this->item($personnel, new MedicalPersonnelTransformer);
        }

        return $this->errorNotFound();
    }

    public function search(ServerRequestInterface $request, ResponseInterface $response)
    {
        if ($request->getQueryParam('query'))
        {
            $personnel = $this->medicalPersonnelRepository->searchPersonnel($request->getQueryParam('query'));

            return $this->collection($personnel, new MedicalPersonnelTransformer);
        }

        return $this->errorNotFound();
    }

    public function update(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        if(!$args['id']){
            return $this->errorNotFound();
        }

        $data = $request->getParsedBody();
        
        $resource = $this->medicalPersonnelRepository->update($data, $args['id']);

        return $this->item($resource, new MedicalPersonnelTransformer);
    }
}
