<?php

namespace Belo\Controllers;

use Belo\User\User;
use Belo\User\UserRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class AuthController extends Controller
{
    /**
     * [$userRepository description]
     * @var [type]
     */
    public $userRepository;

    /**
     * [$authl description]
     * @var [type]
     */
    public $auth;

     /**
     * Create an instance of UserController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->userRepository = new UserRepository;
    }

    /**
     * Login a user
     *
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
     public function login(ServerRequestInterface $request, ResponseInterface $response)
    {
        if ($errors = $this->validate($request, ['username' => 'required', 'password' => 'required|min:2|max:25'])) {
            return $errors;
        }

        if ($token = $this->auth->attempt(
            $request->getParam('username'),
            $request->getParam('password')
            )) {
            return $this->jsonResponse([
                'token' => $token
            ]);
        }

        return $this->errorForbidden();
    }

    /**
     * [logout description]
     * @param  ServerRequestInterface $request  [description]
     * @param  ResponseInterface      $response [description]
     * @return [type]                           [description]
     */
    public function logout(ServerRequestInterface $request, ResponseInterface $response)
    {
        return false;
    }
}
