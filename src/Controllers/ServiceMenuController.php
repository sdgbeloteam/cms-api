<?php

namespace Belo\Controllers;

use Belo\Service\ServiceMenu;
use Belo\Service\ServiceMenuRepository;
use Belo\Service\ServiceMenuTransformer;
use Belo\Requests\ServiceMenuRequests;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ServiceMenuController extends Controller
{
    protected $serviceMenuRepository;
    
     /**
     * Create an instance of UserController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->serviceMenuRepository = new ServiceMenuRepository;
    }

    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        $modelData = $this->serviceMenuRepository->findServices();

        if ($modelData) {
            return $this->collection($modelData, new ServiceMenuTransformer);
        }

        return $this->errorNotFound();
    }

    public function show(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $modelData = $this->serviceMenuRepository->findServicesById($args['service-id']);

        if ($modelData) {
            return $this->collection($modelData, new ServiceMenuTransformer);
        }

        return $this->errorNotFound();
    }

    public function search(ServerRequestInterface $request, ResponseInterface $response)
    {
        if ($request->getQueryParam('query'))
        {
            $service = $this->serviceMenuRepository->findByName($request->getQueryParam('query'));

            if ($service) {
                return $this->collection($service, new ServiceMenuTransformer);
            }
    
            return $this->errorNotFound();
        }

        return $this->errorNotFound();
    }

    public function store(ServerRequestInterface $request, ResponseInterface $response)
    {
        if ($error = $this->validate($request, new ServiceMenuRequests)) {
            return $error;
        }

        $data = $request->getParsedBody();
        $data['service_status']= '1';

        $resource = $this->serviceMenuRepository->save($data);

        return $this->item($resource, new ServiceMenuTransformer);
    }

    public function update(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $data = $request->getParsedBody();

        $resource = $this->serviceMenuRepository->update($data, $args['id']);

        return $this->item($resource, new ServiceMenuTransformer);
    }

    public function delete(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $resource = $this->serviceMenuRepository->delete($args['id']);

        return $this->item($resource, new ServiceMenuTransformer);
    }
}
