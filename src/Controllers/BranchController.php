<?php

namespace Belo\Controllers;

use Belo\Branch\Branch;
use Belo\Branch\BranchRepository;
use Belo\Branch\BranchTransformer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class BranchController extends Controller
{
    /**
     * [$branchRepository description]
     * @var [type]
     */
    public $branchRepository;

    /**
     * Create an instance of BranchControllers
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->branchRepository = new BranchRepository;
    }

    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $this->paginator(
            $this->branchRepository->paginate(
                20,
                $request->getQueryParam('page'),
                $request->getQueryParam('query')
        ), new BranchTransformer);

        return $this->errorNotFound();

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $resource = $this->branchRepository->findByBranchId($args['id']);

        if ($resource) {
            return $this->item(
                $resource, new BranchTransformer
            );
        }

        return $this->errorNotFound();
    }
    public function update(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        if(!$args['id']){
            return $this->errorBadRequests();
        }
        $data = $request->getParsedBody();

        $resource = $this->branchRepository->update($data, $args['id']);

        return $this->item($resource, new BranchTransformer);
    }
    
    public function search(ServerRequestInterface $request, ResponseInterface $response)
    {
        if ($request->getQueryParam('query'))
        {   
            $branch = $this->branchRepository->searchBranchByName($request->getQueryParam('query'));

            return $this->collection($branch, new BranchTransformer);
        }

        return $this->errorNotFound();
    }
}
