<?php

namespace Belo\Controllers;

use Belo\PrivacyPolicy\PrivacyPolicy;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class PrivacyPolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        $data = PrivacyPolicy::first();

        return $this->jsonResponse([
            'message' => $data['content'],
            'created_at' => $data['created_at']
        ]);
    }

    public function update(ServerRequestInterface $request, ResponseInterface $response)
    {
        $data = $request->getParsedBody();
        $data['updated_at'] = date('Y-m-d H:i:s');

        $model = PrivacyPolicy::first();

        $model->update([
            'content' => isset($data['content']) ? $data['content'] : $model['content'],
            'updated_at' => isset($data['updated_at']) ? $data['updated_at'] : $model['content'],
        ]);

        if ($model) {
            return $this->jsonResponse([
                'message' => "Privacy Policy has been updated.",
            ]);
        }
    }

}