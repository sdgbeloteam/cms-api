<?php

namespace Belo\Controllers;

use Belo\Service\ServiceCategory;
use Belo\Service\ServiceCategoryRepository;
use Belo\Service\ServiceCategoryTransformer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ServiceCategoryController extends Controller
{
    /**
    * [$serviceCategoryRepository description]
    * @var [type]
    */
    protected $servicecategoryRepository;

    /**
    * Create an instance of ServiceCategoryController
    */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->serviceCategoryRepository = new ServiceCategoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        $user = $this->auth->user();
        
        return $this->paginator(
            $this->serviceCategoryRepository->paginate(
            20,
            $request->getQueryParam('page')
        ), new ServiceCategoryTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $service = $this->serviceCategoryRepository->findById($args['id']);

        if ($service) {
            return $this->item($service, new ServiceCategoryTransformer);
        }

        return $this->errorNotFound();
    }

    public function store(ServerRequestInterface $request, ResponseInterface $response)
    {
        if ($error = $this->validate($request, ['service_category' => 'required'])) {
            return $error;
        }

        $data = $request->getParsedBody();
        $data['service_status']= '1';

        $resource = $this->serviceCategoryRepository->save($data);

        return $this->item($resource, new ServiceCategoryTransformer);
    }

    public function update(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        if(!$args['id']){
            return $this->errorBadRequests();
        }

        $data = $request->getParsedBody();

        $resource = $this->serviceCategoryRepository->update($data, $args['id']);

        return $this->item($resource, new ServiceCategoryTransformer);
    }

    public function delete(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        if(!$args['id']){
            return $this->errorBadRequests();
        }

        $data = $request->getParsedBody();

        $resource = $this->serviceCategoryRepository->delete($args['id']);

        return $this->item($resource, new ServiceCategoryTransformer);
    }

    public function search(ServerRequestInterface $request, ResponseInterface $response)
    {
        if ($request->getQueryParam('query'))
        {
            $service = $this->serviceCategoryRepository->findByName($request->getQueryParam('query'));

            if ($service) {
                return $this->collection($service, new ServiceCategoryTransformer);
            }
    
            return $this->errorNotFound();
        }

        return $this->errorNotFound();
    }
}
