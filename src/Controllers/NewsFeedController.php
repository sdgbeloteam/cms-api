<?php

namespace Belo\Controllers;

use Belo\NewsFeed\NewsFeed;
use Belo\NewsFeed\NewsFeedRepository;
use Belo\NewsFeed\NewsFeedTransformer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class NewsFeedController extends Controller
{
    /**
     * [$newsFeedRepository description]
     * @var [type]
     */
    public $newsFeedRepository;
     /**
     * Create an instance of UserController
     */
    public function __construct($container)
    {
        parent::__construct($container);
        
        $this->newsFeedRepository = new NewsFeedRepository;

    }

    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        // Filter all active News feed
        $newsFeed = NewsFeed::where('news_status', '=', '2')
            ->orderBy('id', 'desc')
            ->get();

        return $this->collection($newsFeed, new NewsFeedTransformer);
    }

    public function show(ServerRequestInterface $request, ResponseInterface $response, $args)
    {

        $newsFeed = $this->newsFeedRepository->findByNewsFeedId($args['id']);

        if($newsFeed){
            return $this->item($newsFeed, new NewsFeedTransformer);
        }

        return $this->errorNotFound();
    }

    public function update(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        if(!$args['id']){
            return $this->errorNotFound();
        }

        $data = $request->getParsedBody();
        
        $resource = $this->newsFeedRepository->update($data, $args['id']);

        return $this->item($resource, new NewsFeedTransformer);
    }

    public function store(ServerRequestInterface $request, ResponseInterface $response)
    {
        $data = $request->getParsedBody();

        $resource = $this->newsFeedRepository->save($data);

        return $this->item($resource, new NewsFeedTransformer);
    }

}
