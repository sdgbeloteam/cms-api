<?php

namespace Belo\Requests;

class ServiceMenuRequests implements RequestInterface
{
    /**
     * Return the rules
     *
     * @return array [description]
     */
    public function rules()
    {
        return [
            'category_id' => 'required|numeric',
            'service_name' => 'required',
            'description' => 'required',
            'image_link' => 'required',
        ];
    }
}
