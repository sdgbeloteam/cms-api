<?php

namespace Belo\Requests;

interface RequestInterface
{
    /**
     * Return the rules
     *
     * @return array [description]
     */
    public function rules();
}