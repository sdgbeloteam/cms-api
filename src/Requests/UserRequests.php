<?php

namespace Belo\Requests;

class UserRequests implements RequestInterface
{
    /**
     * Return the rules
     *
     * @return array [description]
     */
    public function rules()
    {
        return [
            'username' => 'required|string',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'password' => 'required|min:6|max:20',
            'user_access' => 'required|string',
            'branch_id' => 'required',
        ];
    }
}
