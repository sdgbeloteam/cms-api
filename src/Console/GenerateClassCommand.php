<?php

namespace Belo\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class GenerateClassCommand extends Command
{
    /**
     * [configure description]
     * @return [type] [description]
     */
    protected function configure()
    {
        $this
            ->setName('generate')
            ->addArgument('name', InputArgument::REQUIRED, 'The name of the class.')
            ->setDescription('Scaffolds a new class for convenience.');
    }

    /**
     * [execute description]
     * @param  InputInterface  $input  [description]
     * @param  OutputInterface $output [description]
     * @return [type]                  [description]
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');

        $appPath = __DIR__ . '/../';
        $stubPath = __DIR__ .'/stubs/';
        $classPath = $appPath . ucfirst($name);
        $className = ucfirst($name);

        if (mkdir($classPath))
        {
            $this->createFile(
                $stubPath . 'ModelClass.txt',
                $className,
                $classPath
            );

            $this->createFile(
                $stubPath . 'RepositoryClass.txt',
                $className,
                $classPath,
                'Repository'
            );

            $this->createTransformer(
                $stubPath . 'TransformerClass.txt',
                $className,
                $classPath,
                'Transformer'
            );
        }
    }

    /**
     * [createModel description]
     * @return [type] [description]
     */
    protected function createFile($path, $className, $classPath, $prefix = null)
    {
        $modelStub = file_get_contents($path);

        file_put_contents(
            sprintf('%s/%s%s.php', $classPath, $className, $prefix),
            str_replace('%CLASS_NAME%', $className, $modelStub)
        );
    }

    /**
     * [createModel description]
     * @return [type] [description]
     */
    protected function createTransformer($path, $className, $classPath, $prefix = null)
    {
        $modelStub = file_get_contents($path);
        $modelStub = str_replace('%CLASS_NAME%', $className, $modelStub);
        $modelStub = str_replace('%VAR_NAME%', strtolower($className), $modelStub);

        file_put_contents(
            sprintf('%s/%s%s.php', $classPath, $className, $prefix),
            $modelStub
        );
    }
}