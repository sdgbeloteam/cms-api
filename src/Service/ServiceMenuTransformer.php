<?php

namespace Belo\Service;

use League\Fractal\TransformerAbstract;

class ServiceMenuTransformer extends TransformerAbstract
{
    /**
     * Transform the response from the database to a standard API request
     * @param  patient [description]
     * @return [type]           [description]
     */
    public function transform(ServiceMenu $serviceCategory)
    {

        return [
            'id' => $serviceCategory->id,
            'category_id' => $serviceCategory->category_id,
            'service_name' => $serviceCategory->service_name,
            'description' => htmlspecialchars_decode($serviceCategory->description),
            'image_link' => $serviceCategory->image_link,
            'service_status' => $serviceCategory->service_status,
        ];
    }
}