<?php

namespace Belo\Service;

use Belo\BaseRepository;
use Illuminate\Database\EloquentModel;

class ServiceMenuRepository extends BaseRepository
{
     /**
     * Define the relation of this repository to the model
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return ServiceMenu::class;
    }

    public function findServices()
    {
        return $this->model
            ->where('service_status', '1')
            ->orderBy('id', 'asc')
            ->get();
    }

    public function findServicesById($id)
    {
        return $this->model
            ->where('id', $id)
            ->orderBy('id', 'asc')
            ->get();
    }

    public function save($data)
    {
        return $this
            ->model
            ->create($data);
    }

    public function update($data, $id)
    {
        $model = $this
            ->model
            ->where('id', $id)
            ->first();

        $model->update([
            'category_id' => isset($data['category_id']) ? $data['category_id'] : $model['category_id'],
            'service_name' => isset($data['service_name']) ? $data['service_name'] : $model['service_name'],
            'description' => isset($data['description']) ? $data['description'] : $model['description'],
            'image_link' => isset($data['image_link']) ? $data['image_link'] : $model['image_link'],
        ]);

        return $model;
    }

    public function delete($id)
    {
        $model = $this
            ->model
            ->where('id', $id)
            ->first();

        $model->update([
            'service_status' => '0',
        ]);

        return $model;
    }

    public function findByName($query)
    {
        return $this
            ->model
            ->where('service_name', 'like', "%$query%")
            ->get();
    }
}
