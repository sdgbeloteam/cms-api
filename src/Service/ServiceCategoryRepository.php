<?php

namespace Belo\Service;

use Belo\BaseRepository;
use Illuminate\Database\EloquentModel;

class ServiceCategoryRepository extends BaseRepository
{
    /**
     * Define the relation of this repository to the model
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return ServiceCategory::class;
    }

    /**
     * Find a service by id
     *
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function findById($id)
    {
        return $this
            ->model
            ->where('id', $id)
            ->first();
    }

    /**
     * [paginate description]
     * @param  integer $limit [description]
     * @return [type]         [description]
     */
    public function paginate($limit = 20, $page = 1)
    {
        return $this
            ->model
            ->select([
                'id',
                'service_category',
                'service_status'
            ])
			->where('service_status', '1')
            ->paginate($limit, null, 'page', $page);
    }

    public function save($data)
    {
        return $this
            ->model
            ->create($data);
    }

    public function update($data, $id)
    {
        $model = $this
            ->model
            ->where('id', $id)
            ->first();

        $model->update([
            'service_category' => isset($data['service_category']) ? $data['service_category'] : $model['service_category'],
            'service_status' => isset($data['service_status']) ? $data['service_status'] : $model['service_status'],
        ]);

        return $model;
    }

    public function delete($id)
    {
        $model = $this
            ->model
            ->where('id', $id)
            ->first();

        $model->update([
            'service_status' => '0',
        ]);

        return $model;
    }

    public function findByName($query)
    {
        return $this
            ->model
            ->where('service_category', 'like', "%$query%")
            ->get();
    }
}
