<?php

namespace Belo\Service;

use League\Fractal\TransformerAbstract;

class ServiceCategoryTransformer extends TransformerAbstract
{
    /**
     * Transform the response from the database to a standard API request
     * @param  Service $service [description]
     * @return [type]           [description]
     */
    public function transform(ServiceCategory $service)
    {
        return [
            'id' => (int) $service->id,
            'category' => $service->service_category,
            'service_status' => $service->service_status
        ];
    }
}
