<?php

namespace Belo\Service;

use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'app_service_categories';

    /**
     * The primary key for the model.
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'service_category',
        'service_status',
    ];

    
    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;
}