<?php

namespace Belo\Service;

use Illuminate\Database\Eloquent\Model;

class ServiceMenu extends Model
{
    /**
     * The table associated with the model.
     * @var int
     */
    protected $table = 'app_services';

    /**
     * The primary key for the model.
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'category_id',
        'service_name',
        'description',
        'image_link',
        'service_status',
    ];
    
    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;
}
