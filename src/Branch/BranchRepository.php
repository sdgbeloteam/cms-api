<?php

namespace Belo\Branch;

use Belo\BaseRepository;
use Belo\Branch\Branch;
use Illuminate\Database\EloquentModel;

class BranchRepository extends BaseRepository
{
    /**
     * Define the relation of this repository to the model
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return Branch::class;
    }

    /**
     * Find medical personnel by id
     * @param  char $id
     * @return Patient
     */
    public function findByBranchId($id)
    {
        return $this
            ->model
            ->where('id', $id)
            ->first();
    }

    /**
     * [paginate description]
     * @param  integer $limit [description]
     * @return [type]         [description]
     */
    public function paginate($limit = 10, $page = 1, $query)
    {
        $queryModel = $this
            ->model
            ->select();

        if ($query) {
             $queryModel->where('name', 'like', "%$query%");
        }

        return $queryModel
            ->latest()
            ->paginate($limit, null, 'page', $page);
    }

    public function update($data, $id)
    {
        $model = $this
            ->model 
            ->where('id', $id)
            ->first();

        $model->update([
            'avatar' => isset($data['avatar']) ? $data['avatar'] : $model['avatar'],
            'contact_number' => isset($data['contact_number']) ? $data['contact_number'] : $model['contact_number'],
            'schedules' => isset($data['schedules']) ? $data['schedules'] : $model['schedules'],
        ]);

        return $model;
    }

    public function searchBranchByName($query)
    {
        $queryModel = $this
            ->model
            ->where('name', 'like', "%$query%");
        
        return $queryModel->get();
    }

}