<?php

namespace Belo\Branch;

use League\Fractal\TransformerAbstract;

class BranchTransformer extends TransformerAbstract
{
    /**
     * Transform the response from the database to a standard API request
     * @param  Branch $branch [description]
     * @return [type]           [description]
     */
    public function transform(Branch $branch)
    {
        
        return [
            'id' => $branch->id,
            'name' => $branch->name,
            'address' => $branch->address,
            'contact_number' => $branch->contact_number,
            'avatar' => $branch->avatar,
            'schedules' => $branch->schedules
        ];
    }
}
