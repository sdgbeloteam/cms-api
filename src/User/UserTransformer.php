<?php

namespace Belo\User;

use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * Transform the response from the database to a standard API request
     * @param  User user [description]
     * @return [type]           [description]
     */
    public function transform(User $user)
    {
        $personnel = $user->personnel;
        $medPersonnelId = null;
        $medPersonnelName = null;

        if($personnel) {
            $medPersonnelId = $personnel->id;
            $medPersonnelName = $personnel->name;
        }

        return [
            'id' => $user->username,
            'display_name' => $user->firstname ." ". $user->lastname,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'username' => $user->username,
            'user_theme' => $user->user_theme,
            'has_touchmd' => (bool) $user->has_touchmd,
            'avatar' => ($user->avatar) ? module_path('doctorProfilePicture', $user->avatar): null,
            'created_at' => toIso8601String($user->date_created),
            'position' => $user->position,
            'doctor' => [
                'id' => $medPersonnelId,
                'name' => $medPersonnelName,
            ],
        ];
    }
}