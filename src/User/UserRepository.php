<?php

namespace Belo\User;

use Belo\BaseRepository;
use Illuminate\Database\EloquentModel;

class UserRepository extends BaseRepository
{
     /**
     * Define the relation of this repository to the model
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return User::class;
    }
     /**
     * Find user by id
     * @param  int $id
     * @return User
     */
    public function findUserById($id)
    {
        return $this
            ->model
            ->where('username', $id)
            ->where('user_access', 'IT Admins')
            ->orWhere('user_access', 'Marketing')
            ->first();
    }

    /**
     * [update description]
     * @param  [type] $data [description]
     * @param  [type] $id   [description]
     * @return [type]       [description]
     */
    public function update($data, $id)
    {
        if (isset($data['password'])) {
            $data['password'] = $this->hash($data['password']);
        }

        $model = $this
            ->model
            ->where('username', $id)
            ->first();
            
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * Hash the password using bcrypt
     *
     * @param  string $hassPass
     * @return encrypted and hashed password
    */
    public function hash($password, $cost = 12)
    {
        return password_hash($password, PASSWORD_BCRYPT, ['cost' => $cost]);
    }

    /**
     * find the user with the [username]
     * @param  int $id [description]
     * @return [type]     [description]
     */
    public function findByUserAccess($username, $type = null)
    {
        return $this
            ->model
            ->where('username', $username)
            ->where('user_access', 'IT Admins')
            ->orWhere('user_access', 'Marketing')
            ->first();
    }

     /**
     * find the user with the [username]
     * @param  int $id [description]
     * @return [type]     [description]
     */
    public function findByUsername($username)
    {
        return $this
            ->model
            ->where('username', $username)
            ->where('user_access', 'IT Admins')
            ->orWhere('user_access', 'Marketing')
            ->first();
    }

    
    public function paginate($limit = 20, $page = 1, $query)
    {
        return $this
            ->model
            ->select([
                'username',
                'firstname',
                'middlename',
                'lastname',
                'password',
                'position',
                'avatar',
                'user_access',
                'medical_personnel_id',
                'branch_id',
            ])
            ->with(['userAccess', 'personnel', 'branch'])
            ->whereRaw("MATCH(username, firstname, middlename, lastname, position, user_access, medical_personnel_id, branch_id) AGAINST ('".$query."' IN NATURAL LANGUAGE MODE)")
            ->OrwhereHas('personnel', function($q) use ($query) {
                $q->where('type', 'like', "%$query%");
            })
            ->OrwhereHas('branch', function($b) use ($query) {
                $b->where('name', 'like', "%$query%");
            })
            ->OrwhereHas('userAccess', function($u) use ($query) {
                $u->where('group_name', 'like', "%$query%");
            })
            ->paginate($limit, null, 'page', $page);
    }

    public function save($data)
    {
        $data['password'] = $this->hash($data['password']);
        
        return $this
            ->model
            ->create($data);
    }

    public function updateUser($data, $id)
    {
        $model = $this
            ->model
            ->where('username', $id)
            ->first();
        
        $model->update([
            'firstname' => isset($data['firstname']) ? $data['firstname'] : $model['firstname'],
            'middlename' => isset($data['middlename']) ? $data['middlename'] : $model['middlename'],
            'lastname' => isset($data['lastname']) ? $data['lastname'] : $model['lastname'],
            'password' => isset($data['password']) ? $this->hash($data['password']) : $model['password'],
            'position' => isset($data['position']) ? $data['position'] : $model['position'],
            'avatar' => isset($data['avatar']) ? $data['avatar'] : $model['avatar'],
            'user_access' => isset($data['user_access']) ? $data['user_access'] : $model['user_access'],
            'medical_personnel_id' => isset($data['medical_personnel_id']) ? $data['medical_personnel_id'] : $model['medical_personnel_id'],
            'branch_id' => isset($data['branch_id']) ? $data['branch_id'] : $model['branch_id'],
        ]);
        
        return $model;
    }
}
