<?php

namespace Belo\User;

use Illuminate\Database\Eloquent\Model;

class UserAccess extends Model
{
    /**
     * The table for user accounts.
     */
    protected $table = 'user_access';


}
