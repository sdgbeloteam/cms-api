<?php

namespace Belo\User;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * The primary key for the model.
     * @var string
     */
    protected $primaryKey = 'username';

    
    /**
     * The "type" of the auto-incrementing ID.
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The table for user accounts.
     */
    protected $table = 'users';

    /**
     * @var bool
     */
    public $incrementing = false;
    
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'username',
        'firstname',
        'middlename',
        'lastname',
        'password',
        'position',
        'updated_at',
        'avatar',
        'user_access',
        'medical_personnel_id',
        'branch_id',
    ];

    /**
     * A getter function for accesing id
     * @return [type] [description]
     */
    public function getId()
    {
        return $this->username;
    }

    /**
     * A getter function for accesing id
     * @return [type] [description]
     */
    public function getMedicalPersonnelId()
    {
        return $this->medical_personnel_id;
    }

    /**
     * User's MedicalPersonnel relation
     */
    public function personnel()
    {
        return $this->belongsTo(\Belo\MedicalPersonnel\MedicalPersonnel::class, 'medical_personnel_id', 'id');
    }

    /**
     * User's branch relation
     */
    public function branch()
    {
        return $this->belongsTo(\Belo\Branch\Branch::class, 'branch_id', 'id');
    }
    
    /**
     * User's branch relation
     */
    public function userAccess()
    {
        return $this->belongsTo(\Belo\User\UserAccess::class, 'user_access', 'group_name');
    }

}
