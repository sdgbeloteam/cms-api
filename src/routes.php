<?php

$app->group('/cms-api', function() {
    //login and logout
    $this->post('/auth/login', 'Belo\Controllers\AuthController:login');
    $this->get('/auth/logout', 'Belo\Controllers\AuthController:logout');

    // Service Category
    $this->get('/service-category', 'Belo\Controllers\ServiceCategoryController:index');
    $this->get('/service-category/{id}', 'Belo\Controllers\ServiceCategoryController:show');
    $this->get('/service-category-search', 'Belo\Controllers\ServiceCategoryController:search');
    $this->post('/service-category', 'Belo\Controllers\ServiceCategoryController:store');
    $this->map(['PUT', 'PATCH'], '/service-category/{id}', 'Belo\Controllers\ServiceCategoryController:update');
    $this->map(['PUT', 'PATCH'], '/service-category-delete/{id}', 'Belo\Controllers\ServiceCategoryController:delete');

    // Services Menu
    $this->get('/service-menu', 'Belo\Controllers\ServiceMenuController:index');
    $this->get('/service-menu/{service-id}', 'Belo\Controllers\ServiceMenuController:show');
    $this->get('/service-menu-search', 'Belo\Controllers\ServiceMenuController:search');
    $this->post('/service-menu', 'Belo\Controllers\ServiceMenuController:store');
    $this->map(['PUT', 'PATCH'], '/service-menu-update/{id}', 'Belo\Controllers\ServiceMenuController:update');
    $this->map(['PUT', 'PATCH'], '/service-menu-delete/{id}', 'Belo\Controllers\ServiceMenuController:delete');

    $this->get('/branches', 'Belo\Controllers\BranchController:index');
    $this->get('/branches/{id}', 'Belo\Controllers\BranchController:show');
    $this->get('/branch-search', 'Belo\Controllers\BranchController:search');
    $this->map(['PUT', 'PATCH'], '/branch/{id}', 'Belo\Controllers\BranchController:update');

    // Medical Personnel
    $this->get('/medical-personnel', 'Belo\Controllers\MedicalPersonnelController:index');
    $this->get('/medical-personnel/{id}', 'Belo\Controllers\MedicalPersonnelController:show');
    $this->get('/medical-personnel-search', 'Belo\Controllers\MedicalPersonnelController:search');
    $this->map(['PUT', 'PATCH'], '/medical-personnel-update/{id}', 'Belo\Controllers\MedicalPersonnelController:update');

    // News Feed
    $this->get('/news', 'Belo\Controllers\NewsFeedController:index');
    $this->get('/news/{id}', 'Belo\Controllers\NewsFeedController:show');
    $this->post('/news', 'Belo\Controllers\NewsFeedController:store');
    $this->map(['PUT', 'PATCH'], '/news-update/{id}', 'Belo\Controllers\NewsFeedController:update');

    // Privacy Policy
    $this->get('/policy', 'Belo\Controllers\PrivacyPolicyController:index');
    $this->map(['PUT', 'PATCH'], '/policy', 'Belo\Controllers\PrivacyPolicyController:update');
});

$app->get('/attachments', 'Belo\Controllers\AttachmentController:show');
$app->get('/ping', 'Belo\Controllers\PingController:ping');
