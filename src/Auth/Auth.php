<?php

namespace Belo\Auth;

use Slim\Container;
use Belo\User\UserRepository;
use Psr\Http\Message\ResponseInterface;

class Auth
{
    protected $userRepository;
    protected $builder;
    protected $container;

    /**
     * Create an instance of Auth
     * @param BuilderInterface       $builder        [description]
     * @param UserRepository|null    $userRepository [description]
     * @param Container $container        [description]
     */
    public function __construct(
        BuilderInterface $builder,
        UserRepository $userRepository = null,
        Container $container)
    {
        if (!isset($userRepository)) {
            $userRepository = new UserRepository;
        }

        $this->builder = $builder;
        $this->container = $container;
        $this->userRepository = $userRepository;
    }

    /**
     * [attempt description]
     * @param  [type] $username [description]
     * @param  [type] $password [description]
     * @return [type]           [description]
     */
	public function attempt($username, $password)
	{
        $user = $this->userRepository->findByUsername($username);
        
        // If the user matches the records from the database validate
        // the user credentials and generate the token for the user
        if ($user) {
            if (password_verify($password, $user->password)) {
                return $this->builder->encode($user);
            }
        }

        return false;
	}

    /**
     * Return the user modal instance of the authenticated user
     * @return Model [description]
     */
    public function user()
    {
        $decoded = $this->container->get('jwt.decoded');

        return $this->userRepository->findByUsername($decoded->sub);
    }

    /**
     * Return the id of the authenticated user
     * @return integer [description]
     */
    public function id()
    {
        $decoded = $this->container->get('jwt.decoded');

        return $this->userRepository->findUserById($decoded->sub)->id;
    }

    /**
     * Return the id of the authenticated user
     * @return integer [description]
     */
    public function medicalPersonnelId()
    {
        $decoded = $this->container->get('jwt.decoded');

        return $this
            ->userRepository
            ->findUserById($decoded->sub)
            ->getMedicalPersonnelId();
    }

}
