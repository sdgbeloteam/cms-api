<?php

namespace Belo\Auth;

use Belo\User\User;
use Tuupola\Base62;
use Firebase\JWT\JWT;

class JWTBuilder implements BuilderInterface
{
    protected $settings;

    /**
     * [__construct description]
     * @param [type] $secret [description]
     * @param [type] $ttl    [description]
     */
    public function __construct($settings)
    {
        $this->settings = $settings;
    }
    /**
     * [create description]
     * @param  User   $user [description]
     * @return [type]       [description]
     */
    public function encode(User $user)
    {
        $iat = new \DateTime;
        $exp = new \DateTime(sprintf('now +%d minutes', $this->settings['ttl']));

        $payload = [
            'iat' => $iat->getTimeStamp(),
            'exp' => $exp->getTimeStamp(),
            'jti' => $jti = (new Base62)->encode(random_bytes(16)),
            'sub' => $user->getId()
        ];

        return JWT::encode(
            $payload,
            $this->settings['secret'],
            $this->settings['algo']
        );
    }

    /**
     * [decode description]
     * @param  [type] $token [description]
     * @return [type]        [description]
     */
    public function decode($token)
    {
        return JWT::decode(
            $token,
            $this->settings['secret'],
            [$this->settings['algo']]
        );
    }
}
