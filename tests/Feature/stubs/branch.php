<?php

return [
    'data' => [
        [
            'id',
            'name',
            'address',
            'contact_number',
            'latitude',
            'longitude',
            'email',
            'kiosk_ip',
            'api_port',
        ]
    ],
    'meta' => ['checksum']
];
