<?php

return [
    'data' => [
        [
            'id',
            'pos_description',
            'full_description',
            'units',
            'category',
            'price_list_vat_ex',
            'price_list_vat_inc',
            'standard_cost',
            'barcode',
            'class',
            'department',
            'form',
            'type',
            'procedure_duration'
        ]
    ],
    'meta' => ['indexes', 'pagination']
];
