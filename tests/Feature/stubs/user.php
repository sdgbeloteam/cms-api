<?php

return [
    'data' => [
            'id',
            'display_name',
            'username',
            'user_theme',
            'has_touchmd',
            'avatar',
            'created_at',
            'position'
    ]
];