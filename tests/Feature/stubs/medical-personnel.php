<?php

return [
    'data' => [
        [
            'code',
            'name',
            'gender',
            'branch_code',
            'type',
            'email',
            'mobile',
            'branch_name',
        ]
    ]
];
