<?php

namespace Tests\Feature;

use Tests\BaseTestCase;

class MedicalPersonnelTest extends BaseTestCase
{
    use Support\AuthStubTrait;

    protected $withMiddleware = true;

    public function getApiStructure()
    {
        return require('stubs/medical-personnel.php');
    }

    public function test_index()
    {
        $branch = \Belo\Branch\Branch::first();

        $response = $this->runApp('GET', '/api/medical-personnel?branch='. $branch->id);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJsonStructure($this->getApiStructure());
    }

    public function test_show()
    {
        $personnel = \Belo\MedicalPersonnel\MedicalPersonnel::first();
        
        $response = $this->runApp('GET', '/api/medical-personnel/' . $personnel->id);

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_store()
    {
        $response = $this->runApp('POST', '/api/medical-personnel', [
            'id' => 'AES'. date("i") ,
            'type' => 'Aesthetician',
            'status' => 'active',
            'name' => 'Princess Yuu Shinohara',
            'title' => 'Aes.',
            'avatar' => 'data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAIAAAACUFjqAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAADUlEQVQYlWNgGAWkAwABNgABxYufBwAAAABJRU5ErkJggg==',
            'gender' => 'female',
            'specialization' => 'Aesthetician',
            'education' => '',
            'category' => 'na',
            'branch' => 'Greenhills',
            'branch_code' => '04',
            'work_days' => ''
        ]);

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_update()
    {
        $personnel = \Belo\MedicalPersonnel\MedicalPersonnel::where('id', 'like', 'AES%')->orderBy('id', 'desc')->first();

        $response = $this->runApp('PUT', '/api/medical-personnel/'. $personnel->id, [
            'type' => 'Aesthetician',
            'status' => 'active',
            'name' => 'Princess Yuu Shinohara',
            'title' => 'Aes.',
            'avatar' => 'data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAIAAAACUFjqAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAADUlEQVQYlWNgGAWkAwABNgABxYufBwAAAABJRU5ErkJggg==',
            'gender' => 'female',
            'specialization' => 'Aesthetician',
            'education' => '',
            'category' => 'na',
            'branch' => 'Greenhills, Greenbelt',
            'branch_code' => '04',
            'work_days' => ''
        ]);

        $this->assertEquals(200, $response->getStatusCode());
    }
}