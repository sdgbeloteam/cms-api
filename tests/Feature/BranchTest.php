<?php

namespace Tests\Feature;

use Ramsey\Uuid\Uuid;
use Tests\BaseTestCase;

class BranchTest extends BaseTestCase
{
    public function getApiStructure()
    {
        return require('stubs/branch.php');
    }

    public function test_index()
    {
        $response = $this->runApp('GET', '/api/branches');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJsonStructure($this->getApiStructure());
    }

    public function test_index_checksum()
    {
        $branches = \Belo\Branch\Branch::where('isSync', '=', '1')
            ->get()
            ->filter(function($branch) {
                return filter_var($branch->ip, FILTER_VALIDATE_IP) !== false;
            });

        $response = $this->runApp('GET', '/api/branches');

        $checksum = $this->decodeResponseJson()['meta']['checksum'];

        $this->assertEquals($checksum,md5(serialize($branches->toArray())));
    }

    public function test_show()
    {
        $branch = \Belo\Branch\Branch::first();

        $response = $this->runApp('GET', '/api/branches/'.$branch->id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_store()
    {
        $response = $this->runApp('POST', '/api/branches', [
            'id' => Uuid::uuid4()->toString(),
            'name' => 'Mindanao',
            'address' => 'Lanao Del Norte',
            'contact_number' => '09999999999',
            'latitude' => '14.531234',
            'longitude' => '120.980144',
            'email' => 'lanao@belomed.com',
            'kiosk_ip' => '192.168.100.100',
            'api_port' => '3308'
        ]);

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_update()
    {
        $branch = \Belo\Branch\Branch::orderBy('created_at', 'desc')->first();

        $response = $this->runApp('PUT', '/api/branches/' . $branch->id, [
            'name' => 'Updated Branch name',
            'address' => 'Tacloban Mindanao',
            'contact_number' => '09999988888',
            'latitude' => '14.53123422',
            'longitude' => '120.98014433',
            'email' => 'mindanao@belomed.com',
            'kiosk_ip' => '192.168.100.100',
            'api_port' => '3309'
        ]);

        $this->assertEquals(200, $response->getStatusCode());
    }

}
