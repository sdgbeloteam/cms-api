<?php

namespace Tests\Feature;

use Tests\BaseTestCase;

class ServiceTest extends BaseTestCase
{
    use Support\AuthStubTrait;

    protected $withMiddleware = true;

    public function getApiStructure()
    {
        return require('stubs/service.php');
    }

    public function test_index()
    {
        $response = $this->runApp('GET', '/api/services');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJsonStructure($this->getApiStructure());
    }

    public function test_show()
    {
        $service = \Belo\Service\Service::first();

        $response = $this->runApp('GET', '/api/services/' . $service->aid);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson($this->getBody(), [
            'id' => 1,
            'full_description' => $service->full_description,
        ]);
    }

    public function test_error_query_and_starts_with()
    {
        $response = $this->runApp('GET', '/api/services?query=Foo+Bar&starts_with=B');
        $this->assertEquals(400, $response->getStatusCode());
    }

    public function test_error_starts_with_query()
    {
        $response = $this->runApp('GET', '/api/services?starts_with=BOB+ONG');
        $this->assertEquals(400, $response->getStatusCode());
    }

    public function test_starts_with_query()
    {
        $service = \Belo\Service\Service::selectRaw('SUBSTR(pos_description, 1, 1) as alpha_index')
            ->groupBy('alpha_index')
            ->first();

        $response = $this->runApp('GET', '/api/services?starts_with=' . $service->alpha_index);

        $this->assertNotEmpty($this->decodeResponseJson()['data']);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
