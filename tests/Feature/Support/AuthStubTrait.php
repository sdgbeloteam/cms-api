<?php

namespace Tests\Feature\Support;

trait AuthStubTrait
{
    public $token;

    /**
     * [setUp description]
     */
    public function setUp()
    {
        $r = $this->runApp('POST', '/api/auth/login', [
            'username' => 'jgayod',
            'password' => 'belomedical'
        ]);

        $this->token = $this->decodeResponseJson()['token'];
    }
}
