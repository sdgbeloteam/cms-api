<?php

namespace Tests\Feature;

use Tests\BaseTestCase;

class UserTest extends BaseTestCase
{
    use Support\AuthStubTrait;

    protected $withMiddleware = true;

    public function getApiStructure()
    {
        return require('stubs/user-show.php');
    }

    public function test_index()
    {
        $response = $this->runApp('GET', '/api/users');
        
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJsonStructure($this->getApiStructure());
    }

    public function test_show()
    {
        $user = \Belo\User\User::orderBy('username', 'desc')->first();

        $response = $this->runApp('GET', '/api/users/'. $user->username);
        
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJsonStructure(require('stubs/user.php'));
    }

    public function test_store_reqiured_fields()    
    {
        $branch = \Belo\Branch\Branch::first();

        $response = $this->runApp('POST', '/api/users', [
            'username' => 'test'. date('mdi'),
            'firstname' => 'FirstName'. date('md'),
            'middlename' => '',
            'lastname' => 'Last Nmae'. date('md'),
            'password' => '1234567',
            'position' => '',
            'avatar' => '',
            'user_access' => 'IT Admin',
            'medical_personnel_id' => '',
            'branch_id' => $branch->id,
        ]);
        
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJsonStructure(require('stubs/user.php'));
    }

    public function test_store_error()
    {
        $branch = \Belo\Branch\Branch::first();

        $response = $this->runApp('POST', '/api/users', [
            'username' => 'test'. date('mdi'),
            'firstname' => 'FirstName'. date('mdi'),
            'middlename' => '',
            'lastname' => 'Last Nmae'. date('md'),
            'password' => '',
            'position' => '',
            'avatar' => '',
            'user_access' => 'IT Admin',
            'medical_personnel_id' => '',
            'branch_id' => $branch->id,
        ]);
        
        $this->assertEquals(422, $response->getStatusCode());
    }

    public function test_update()
    {
        $branch = \Belo\Branch\Branch::first();
        $user = \Belo\User\User::orderBy('username', 'desc')->first();

        $response = $this->runApp('PUT', '/api/users/'. $user->username, [
            'firstname' => 'FirstName'. date('mdi'),
            'avatar' => 'data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAIAAAACUFjqAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAADUlEQVQYlWNgGAWkAwABNgABxYufBwAAAABJRU5ErkJggg==',
        ]);
        
        $this->assertEquals(200, $response->getStatusCode());
    }
}
