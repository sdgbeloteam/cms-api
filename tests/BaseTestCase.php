<?php

namespace Tests;

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Environment;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use PHPUnit_Util_InvalidArgumentHelper;
use Illuminate\Database\Capsule\Manager;
use PHPUnit\Framework\Assert as PHPUnit;

/**
 * This is an example class that shows how you could set up a method that
 * runs the application. Note that it doesn't cover all use-cases and is
 * tuned to the specifics of this skeleton app, so if your needs are
 * different, you'll need to change it.
 */
abstract class BaseTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * Use middleware when running application?
     *
     * @var bool
     */
    protected $withMiddleware = false;

    /**
     * Slim container
     * @var [type]
     */
    protected $container;

    /**
     * [$response description]
     * @var [type]
     */
    protected $response;

    /**
     * [$token description]
     * @var [type]
     */
    public $token;

    /**
     * Process the application given a request method and URI
     *
     * @param string $requestMethod the request method (e.g. GET, POST, etc.)
     * @param string $requestUri the request URI
     * @param array|object|null $requestData the request data
     * @return \Slim\Http\Response
     */
    public function runApp($requestMethod, $requestUri, $requestData = null)
    {
        error_reporting(E_ALL);

        ini_set('display_errors', FALSE);

        // Create a mock environment for testing with
        $environment = Environment::mock(
            [
                'REQUEST_METHOD' => $requestMethod,
                'REQUEST_URI' => $requestUri,
            ]
        );

        if (property_exists($this, 'token') && $this->token) {
            $environment['HTTP_AUTHORIZATION'] = 'Bearer ' . $this->token;
        }

        // Set up a request object based on the environment
        $request = Request::createFromEnvironment($environment);

        // Add request data, if it exists
        if (isset($requestData)) {
            $request = $request->withParsedBody($requestData);
        }

        if (file_exists(__DIR__ . '/../.env'))
        {
            $dotenv = new \Dotenv\Dotenv(__DIR__.'/../');
            $dotenv->load();
        }

        // Set up a response object
        $response = new Response();

        // Use the application settings
        $settings = require __DIR__ . '/../src/settings.php';

        // Instantiate the application
        $app = new App($settings);

        $this->container = $container = $app->getContainer();

        $manager = new Manager;

        $manager->addConnection($settings['settings']['database']);
        $manager->setEventDispatcher(new Dispatcher(new Container));

        $manager->setAsGlobal();
        $manager->bootEloquent();

        $container['db'] = function() use ($manager) {
            return $manager->getDatabaseManager();
        };

        // Set up dependencies
        require __DIR__ . '/../src/dependencies.php';

        // Register middleware
        if ($this->withMiddleware) {
            require __DIR__ . '/../src/middleware.php';
        }

        // Register routes
        require __DIR__ . '/../src/routes.php';

        // Process the application
        $response = $app->process($request, $response);

        // Return the response
        return $this->response = $response;
    }

    /**
       * Assert that the response has a given JSON structure.
       *
       * @param  array|null  $structure
       * @param  array|null  $responseData
       * @return $this
       */
    public function assertJsonStructure(array $structure = null, $responseData = null)
    {
        if (is_null($structure)) {
            return $this->assertJson();
        }

        if (is_null($responseData)) {
            $responseData = $this->decodeResponseJson();
        }

        foreach ($structure as $key => $value) {
            if (is_array($value) && $key === '*') {
                PHPUnit::assertInternalType('array', $responseData);

                foreach ($responseData as $responseDataItem) {
                    $this->assertJsonStructure($structure['*'], $responseDataItem);
                }
            } elseif (is_array($value)) {
                PHPUnit::assertArrayHasKey($key, $responseData);

                $this->assertJsonStructure($structure[$key], $responseData[$key]);
            } else {
                PHPUnit::assertArrayHasKey($value, $responseData);
            }
        }

        return $this;
    }

    /**
     * Asserts that a string is a valid JSON string.
     *
     * @param string $actualJson
     * @param string $message
     */
    public static function assertJson($actualJson, $message = '')
    {
        if (!is_string($actualJson)) {
            throw PHPUnit_Util_InvalidArgumentHelper::factory(1, 'string');
        }

        static::assertThat($actualJson, static::isJson(), $message);
    }

    /**
    * Validate and return the decoded response JSON.
    *
    * @return array
    */
    public function decodeResponseJson()
    {
        $decodedResponse = json_decode($this->response->getBody(), true);

        if (is_null($decodedResponse) || $decodedResponse === false) {
            if ($this->exception) {
                throw $this->exception;
            } else {
                PHPUnit::fail('Invalid JSON was returned from the route.');
            }
        }

        return $decodedResponse;
    }

    /**
     * Returns the body without casting some magic spell
     *
     * @return [type] [description]
     */
    public function getJsonBody()
    {
        return $this->decodeResponseJson($this->response);
    }

    /**
     * Returns the body without casting some magic spell
     *
     * @return [type] [description]
     */
    public function getBody()
    {
        return array_filter(explode("\n", (string) $this->response))[3];
    }

    /**
     * Assert that the response is a superset of the given JSON.
     *
     * @param  array  $data
     * @return $this
     */
    public function assertJsonSubset(array $data)
    {
        PHPUnit::assertArraySubset(
            $data, $this->decodeResponseJson(), false, $this->assertJsonMessage($data)
        );

        return $this;
    }

    /**
     * Get the assertion message for assertJson.
     *
     * @param  array  $data
     * @return string
     */
    protected function assertJsonMessage(array $data)
    {
        $expected = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

        $actual = json_encode($this->decodeResponseJson(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

        return 'Unable to find JSON: '.PHP_EOL.PHP_EOL.
            "[{$expected}]".PHP_EOL.PHP_EOL.
            'within response JSON:'.PHP_EOL.PHP_EOL.
            "[{$actual}].".PHP_EOL.PHP_EOL;
    }

    public function tearDown()
    {
        Manager::disconnect('connection');
    }
}
