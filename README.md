# API

The api used by `Belo: BMS,SMS,MD Module,PIS`  applications.

### Requirements

* PHP >= 5.6
* MySQL 5.5
* php5.6-gd

### Installation

Clone the repository and install all dependencies

    git clone git@bitbucket.org/sdgbeloteam/beloapi-v2.git

### Branch installation

* Make sure that the `.env.example` is copied as `.env`
* Change the `JWT_SECRET` to a random alphanumeric
* Change the `STORAGE_HOST` to the desired configuration

### Tests

    vendor/bin/phpunit

> To run feature and units tests execute the phpunit

### Routes Checklist

#### Patients

Endpoint | Methods Allowed |  Description
------------ | ------------- | ------------
patients?search={} | GET| Generates all patient based on query
patients/{patient_id} | GET| Generate a patient information based on its patient_id
patients | POST | Add a patient profile
patients/{patient_id} | PUT,PATCH | Update a patient profile
patients/{patient_id}/avatar | POST | Add a patient's profile picture

###### Patients - archives
Endpoint | Methods Allowed |  Description
------------ | ------------- | ------------
patients/{patient_id}/archives | GET| Generate the patient's archive albums of uploaded documents
patients/{patient_id}/archives/{id} | GET| Generate a patients archive on a specific album
patients/{patient_id}/archives | POST | Create a new patient archive album
patients/{patient_id}/archives/{id} | PUT,PATCH| Update an existing patient archive album. You can either add new image or delete an image

###### Patients - before and after
Endpoint | Methods Allowed |  Description
------------ | ------------- | ------------
patients/{patient_id}/before-and-after | GET| Generate the patient's before and after albums
patients/{patient_id}/before-and-after/{id} | GET| Generate the patient's specific before and after albums
patients/{patient_id}/before-and-after | POST | Create a new patient before and after album
patients/{patient_id}/before-and-after/{id} | PUT,PATCH | Update an existing patient before and after  album. You can either add new image or delete an image

###### Patients - consent forms
Endpoint | Methods Allowed |  Description
------------ | ------------- | ------------
patients/{patient_id}/consent-forms | GET| Generate the patient's consent forms
patients/{patient_id}/consent-forms/{id} | GET| Generate the patient's specific consent form
patients/{patient_id}/consent-forms | POST | Create a consent form for a patient

#### Branches
Endpoint | Methods Allowed |  Description
------------ | ------------- | ------------
branches | GET| Generate the branches
branches/{id} | GET| Generate the a specific
branches | POST | Create a new branch
branches/{id} | PUT,PATCH | Update an existing branch details

#### Before and After
Endpoint | Methods Allowed |  Description
------------ | ------------- | ------------
before-and-after | GET| Generate before and after gallery for viewing
before-and-after/{id} | GET| Generate a specific album of before and after
before-and-after | POST | Create a new album
before-and-after/{id} | PUT,PATCH | Update an existing album contents

#### Faqs
Endpoint | Methods Allowed |  Description
------------ | ------------- | ------------
faqs | GET| Generate faq for viewing
faqs/{id} | GET| Generate a specific faq
faqs | POST | Create a new faq
faqs/{id} | PUT,PATCH | Update an faq
email-faq | POST | Send the contents of the faq via email

#### Services
Endpoint | Methods Allowed |  Description
------------ | ------------- | ------------
services | GET| Generate service menu for viewing
services/{id} | GET| Generate a specific service menu
services | POST | Create a new service menu
services/{id} | PUT,PATCH | Update a service menu

#### Medical Personnel
Endpoint | Methods Allowed |  Description
------------ | ------------- | ------------
medical-personnel | GET| Generate all medical personnels for viewing
medical-personnel/{id} | GET| Generate a specific medical personnels
medical-personnel | POST | Create a new medical personnel
medical-personnel/{id} | PUT,PATCH | Update a medical personnel

#### Appointments

Endpoint | Methods Allowed |  Description
------------ | ------------- | ------------
appointments | POST | Create an appointment
appointments/{id} | GET | Open a specific appointment
appointments/{id} | PUT,PATCH | Update an appointment
appointments/{id}/appointment-status | PUT,PATCH | Update an appointment's appointment status only
appointments/{id}/confirmation-status | PUT,PATCH | Update an appointment's confirmation status only
appointments/{id}/add-procedure | POST | Insert an additional procedure on an existing appointment

###### Appointments - Branches
Endpoint | Methods Allowed |  Description
------------ | ------------- | ------------
branches/{id}/appointments?date={YYYY-mm-dd} | GET| Generate the branch list of appointments based on the date

###### Appointments - status
Endpoint | Methods Allowed |  Description
------------ | ------------- | ------------
appointment-status | GET| Generate all status of appointments for viewing
appointment-status/{id} | GET| Generate a specific appointment status
appointment-status | POST | Create a new appointment status
appointment-status/{id} | PUT,PATCH | Change the appointment status details

### Routes Documentation
